﻿using Microsoft.EntityFrameworkCore;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using Todo.Controllers;
using Todo.DataAccess;
using Xunit;

namespace Todo.Tests
{
    public class HomeControllerTest
    {
        private IQueryable<TodoModel> data;

        public HomeControllerTest()
        {
            data = new List<TodoModel>
            {
                new TodoModel { Id = 1, Task = "BBB" },
                new TodoModel { Id = 2, Task= "ZZZ" },
                new TodoModel { Id = 3, Task= "AAA" },
            }.AsQueryable();
        }

        [Fact]
        public void GetDataTest()
        {
            // create a mock DbSet exposing both DbSet and IQueryable interfaces for setup
            var mockSet = Substitute.For<DbSet<TodoModel>, IQueryable<TodoModel>>();

            // setup all IQueryable methods using what you have from "data"
            ((IQueryable<TodoModel>)mockSet).Provider.Returns(data.Provider);
            ((IQueryable<TodoModel>)mockSet).Expression.Returns(data.Expression);
            ((IQueryable<TodoModel>)mockSet).ElementType.Returns(data.ElementType);
            ((IQueryable<TodoModel>)mockSet).GetEnumerator().Returns(data.GetEnumerator());

            // do the wiring between DbContext and DbSet
            var mockContext = Substitute.For<ITodoContext>();
            mockContext.Todos.Returns(mockSet);
            var controller = new HomeController(mockContext);
            controller.GetData();

            mockSet.Received().ToArray();
        }
    }

    public interface ICalculator
    {
        int Add(int a, int b);
        string Mode { get; set; }
        event Action PoweringUp;
    }
}
