﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Todo.Ajaxified.Utils
{
	public class MyAuthAttribute : IAuthorizationFilter
	{
		ILoggerFactory logger;
		public MyAuthAttribute(ILoggerFactory logger)
		{
			this.logger = logger;
		}

		public void OnAuthorization(AuthorizationFilterContext context)
		{
			if (context.HttpContext.Request.Headers.ContainsKey("auth-token") == false)
			{
				context.HttpContext.Response.StatusCode = 991;
				context.Result = new JsonResult(new
				{
					message = "Authentication not present - logger = " + logger.ToString()
				});
				return;
			}

			var token = context.HttpContext.Request.Headers["auth-token"][0];
			if (token != "muhaha")
			{
				context.Result = new JsonResult(new
				{
					message = "Authentication token not valid"
				});
			}
		}
	}
}