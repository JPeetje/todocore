﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Todo.Ajaxified.DataAccess;
using Microsoft.Extensions.Caching.Memory;
using System.Threading;
using Microsoft.Extensions.Primitives;

namespace Todo.Ajaxified.Controllers
{
	[Route("api/[controller]")]
	public class TodoController : Controller
	{
		IMemoryCache cache;
		public TodoController(IMemoryCache cache)
		{
			this.cache = cache;
		}

		[HttpGet]
		public IEnumerable<TodoModel> Get()
		{
			return new List<TodoModel>()
			{
				new TodoModel() { Id = 4, Task = "Vuilnis" },
				new TodoModel() { Id = 8, Task = "Hondje uitlaten" },
				new TodoModel() { Id = 15, Task = "Sporten" }
			};
		}

		[HttpGet]
		[Route("test")]
		public string Test()
		{
			string datum;
			if(!cache.TryGetValue("datum", out datum))
			{
				var cts = new CancellationTokenSource();

				datum = DateTime.Now.ToString();

				var options = new MemoryCacheEntryOptions();
				options.AddExpirationToken(new CancellationChangeToken(cts.Token));
				cache.Set("datum", datum, options);

				Task.Factory.StartNew(() =>
				{
					Thread.Sleep(3000);
					cts.Cancel();
				});
			}

			return datum;
		}
	}
}
