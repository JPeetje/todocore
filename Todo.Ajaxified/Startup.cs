﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Todo.Ajaxified.Utils;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Http;
using System.Net.WebSockets;
using System.Threading;

namespace Todo.Ajaxified
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
			services.AddMemoryCache();

			// Add framework services.
			services.AddMvc(options =>
			{
				
				//options.OutputFormatters.Clear();
				//options.OutputFormatters.Add(new XmlSerializerOutputFormatter());
				//options.Filters.Add(new MyAuthAttribute());
				options.Filters.Add(typeof(MyAuthAttribute));
			})
			.AddJsonOptions(options =>
			{
				options.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects;
			});
		}

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

			app.UseMiddleware(typeof(ErrorHandlingMiddleware));

			//app.UseDeveloperExceptionPage();

			//var webSocketOptions = new WebSocketOptions()
			//{
			//	KeepAliveInterval = TimeSpan.FromSeconds(120),
			//	ReceiveBufferSize = 4 * 1024
			//};
			//app.UseWebSockets(webSocketOptions);
			//app.Use(async (context, next) =>
			//{
			//	if (context.Request.Path == "/ws")
			//	{
			//		if (context.WebSockets.IsWebSocketRequest)
			//		{
			//			WebSocket webSocket = await context.WebSockets.AcceptWebSocketAsync();
			//			await Echo(context, webSocket);
			//		}
			//		else
			//		{
			//			context.Response.StatusCode = 400;
			//		}
			//	}
			//	else
			//	{
			//		await next();
			//	}

			//});

			app.UseStatusCodePagesWithRedirects("~/errors/{0}.html");

			app.UseStaticFiles();

			app.UseMvc();
        }

		private async Task Echo(HttpContext context, WebSocket webSocket)
		{
			var buffer = new byte[1024 * 4];
			WebSocketReceiveResult result = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
			while (!result.CloseStatus.HasValue)
			{
				await webSocket.SendAsync(new ArraySegment<byte>(buffer, 0, result.Count), result.MessageType, result.EndOfMessage, CancellationToken.None);

				result = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
			}
			await webSocket.CloseAsync(result.CloseStatus.Value, result.CloseStatusDescription, CancellationToken.None);
		}
	}
}
