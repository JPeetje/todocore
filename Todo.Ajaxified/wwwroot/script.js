﻿

let socket;
function connect() {

	console.log('socket openen...');

	socket = new WebSocket('ws://localhost:58002/ws');
	socket.addEventListener('open', () => {
		console.log('socket geopend, berichtje sturen');
		socket.send('hoiiiiiiii');
	});
	socket.addEventListener('close', () => console.log('socket gesloten'));
	socket.addEventListener('message', e => {
		console.log('bericht ontvangen:', e.data, e.message);
	});
	socket.addEventListener('data', e => {
		console.log('data ontvangen:', e.data, e.message);
	});
}

function sendMessage() {
	let message = document.querySelector('#input-message').value;
	console.log('sending message:', message);
	socket.send(message);
}

function getTodos() {

	let headers = new Headers();
	headers.append('auth-token', 'muhaha');
	let req = {
		headers: headers
	};

	fetch('/api/todo', req).then(res => res.json()).then(todos => {
		$('#todolist').children().remove();

		for (let todo of todos) {
			$('<li>').text(todo.task).appendTo('#todolist');
		}
	});
}