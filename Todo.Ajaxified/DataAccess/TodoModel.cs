﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Todo.Ajaxified.DataAccess
{
    public class TodoModel
    {
        public int Id { get; set; }

		[Required]
        public string Task { get; set; }

        [DataType(DataType.Date)]
		[Required]
        public DateTime DueDate { get; set; }
    }
}