﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace VanNiksOpbouwenTest
{
	public class Program
	{
		public static void Main(string[] args)
		{
			var config = new ConfigurationBuilder().Build();

			var host = new WebHostBuilder()
				.UseContentRoot(Directory.GetCurrentDirectory())
				.UseConfiguration(config)
				.UseStartup<Startup>()
				.UseKestrel()
				.Build();

			host.Run();
		}
	}
}
