﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using VanNiksOpbouwenTest.DataAccess;

namespace VanNiksOpbouwenTest.Migrations
{
    [DbContext(typeof(ContactContext))]
    [Migration("20170716180340_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("VanNiksOpbouwenTest.Models.ContactModel", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Leeftijd");

                    b.Property<string>("Naam");

                    b.HasKey("Id");

                    b.ToTable("Contacts");
                });
        }
    }
}
