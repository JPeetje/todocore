﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VanNiksOpbouwenTest.Models
{
    public class ContactModel
    {
		public int Id { get; set; }

		public string Naam { get; set; }

		public int Leeftijd { get; set; }
	}
}
