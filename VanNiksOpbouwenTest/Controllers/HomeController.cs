﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VanNiksOpbouwenTest.DataAccess;

namespace VanNiksOpbouwenTest.Controllers
{
	public class HomeController : Controller
	{
		ContactContext context;
		public HomeController(ContactContext context)
		{
			this.context = context;
		}
		public IActionResult Index()
		{
			return View(context.Contacts.ToList());

			context.SaveChangesAsync();
		}
	}
}