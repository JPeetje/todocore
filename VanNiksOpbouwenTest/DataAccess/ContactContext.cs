﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VanNiksOpbouwenTest.Models;

namespace VanNiksOpbouwenTest.DataAccess
{
    public class ContactContext : DbContext
    {
		public ContactContext(DbContextOptions options) : base(options)
        {

		}
		public ContactContext()
		{

		}

		public DbSet<ContactModel> Contacts { get; set; }
    }
}
