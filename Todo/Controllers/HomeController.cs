﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Todo.DataAccess;
using Todo.Utils;

namespace Todo.Controllers
{
	public class HomeController : Controller
	{
		private ITodoContext context;

		public HomeController(ITodoContext context)
		{
			this.context = context;
		}

		public IActionResult Index()
		{
			ViewData["bla"] = "<script>alert('yay');</script>";
			return View(context.Todos.ToList());
		}

		public IActionResult Ophoog()
		{
			var nu = HttpContext.Session.Get<int>("tellertje");
			HttpContext.Session.Set<int>("tellertje", nu + 1);

			TempData["tellertje2"] = nu + 1;

			return Content("teller staat nu op " + (nu + 1));
		}

		[HttpPost]
		public IActionResult Index(TodoModel newTodo)
		{
			if (ModelState.IsValid)
			{
				context.Todos.Add(newTodo);
				context.SaveChanges();
			}

			return View(context.Todos.ToList());
		}

		public IActionResult GetData()
		{
			return Json(context.Todos.ToArray());
		}
	}
}
