﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Todo.Utils;

namespace Todo.Controllers
{
    public class ContactController : Controller
    {
		public IActionResult Index()
		{
			//Session
			var nu = HttpContext.Session.Get<int>("tellertje");
			return Content("en vanaf contact staat hij nu op " + nu + " en de tempdata teller op: " + TempData["tellertje2"]);
		}
	}
}
