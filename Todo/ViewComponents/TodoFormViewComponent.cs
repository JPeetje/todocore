﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Todo.DataAccess;

namespace Todo.ViewComponents
{
	public class TodoFormViewComponent : ViewComponent
	{
		public async Task<IViewComponentResult> InvokeAsync()
		{
			return View(new TodoModel() { Task = "Bla" });
		}
	}
}
