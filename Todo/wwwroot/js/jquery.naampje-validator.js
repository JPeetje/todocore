﻿~function ($) {
	$.validator.addMethod('naampje', (value, element, params) => {
		return value.includes('*') === false;
	});

	$.validator.unobtrusive.adapters.addBool('naampje');
}(jQuery);