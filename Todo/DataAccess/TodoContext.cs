﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Todo.DataAccess
{
    public class TodoContext : DbContext, ITodoContext
    {
        public TodoContext(DbContextOptions options) : base(options)
        {

        }
        public TodoContext()
        {

        }

        public DbSet<TodoModel> Todos { get; set; }
    }
}