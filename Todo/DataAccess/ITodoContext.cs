﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Todo.DataAccess
{
    public interface ITodoContext
    {
        DbSet<TodoModel> Todos { get; }
        int SaveChanges();
    }
}