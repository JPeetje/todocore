﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using IdentityGoogleTest.Models;
using Microsoft.AspNetCore.Authorization;

namespace IdentityGoogleTest.Controllers
{
    public class HomeController : Controller
    {
		private UserManager<ApplicationUser> userManager;
		public HomeController(UserManager<ApplicationUser> userManager)
		{
			this.userManager = userManager;
		}
        public IActionResult Index()
        {
            return View();
        }

		[Authorize]
		public async Task<IEnumerable<Claim>> Lijstje()
		{
			var user = await userManager.GetUserAsync(this.User);
			var claims = await userManager.GetClaimsAsync(user);

			return claims.ToList();
		}

		[Authorize(Policy = "Special")]
		public string Test()
		{
			return "Yeah succes!";
		}

		public async Task<string> AddClaim()
		{
			var user = await userManager.GetUserAsync(User);
			await userManager.AddClaimAsync(user, new Claim("First Name", "Juan Pedo"));
			return "Claim added.";
		}

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
